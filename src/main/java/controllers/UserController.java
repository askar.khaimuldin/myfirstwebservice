package controllers;

import domain.User;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.UserInteractor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("users")
public class UserController {

    private UserInteractor userInteractor;

    public UserController() {
        userInteractor = new UserInteractor();
    }

    @GET
    public String hello() {
        return "Hello world!";
    }

    @GET
    @Path("/{param}")
    public Response getUserByID(@PathParam("param") long id) {
        User user = userInteractor.getUserByID(id);
        if (user == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User does not exist!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(user)
                    .build();
        }
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response createNewUser(@FormDataParam("name") String name,
                                  @FormDataParam("surname") String surname,
                                  @FormDataParam("username") String username,
                                  @FormDataParam("password") String password,
                                  @FormDataParam("birthday") String birthday){
        userInteractor.addUser(name, surname, username, password, birthday);
        return Response.
                status(Response.Status.CREATED)
                .entity("User created successfully!")
                .build();
    }
}
