package services;

import domain.User;
import repositories.entities.UserRepository;
import repositories.interfaces.IEntityRepository;

import java.sql.Date;
import java.util.LinkedList;

public class UserInteractor {
    private IEntityRepository userRepo;

    public UserInteractor() {
        userRepo = new UserRepository();
    }

    public User getUserByID(long id) {
        String sql = "SELECT * FROM users WHERE id = " + id;
        LinkedList<User> users = (LinkedList<User>) userRepo.query(sql);
        return users.isEmpty() ? null : users.get(0);
    }

    public void addUser(String name, String surname, String username,
                        String password, String birthday) {
        User user = new User(name, surname, username, password, Date.valueOf(birthday));
        userRepo.add(user);
    }
}
